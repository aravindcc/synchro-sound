from tempfile import NamedTemporaryFile
import shutil
import csv

filename = 'wpd_datasets-2.csv'
filename_out = 'wpd_datasets-2-out.csv'
tempfile = NamedTemporaryFile(mode='w', delete=False)

def transform_val(val):
    vals = [
        1.16,
        1.126,
        1.109,
        1.079,
        1.066,
        1.053,
        1.04,
        1.027,
        1.015,
        1.004,
        0.985,
        0.605,
        0.62,
        0.625,
        0.65,
        0.69,
        0.71,
        0.72,
        0.775,
        0.87
    ]
    for ref in vals:
        if val > ref - 0.002 and val < ref + 0.002:
            return ref
    raise Exception()

with open(filename, 'r') as csvfile, tempfile:
    reader = csv.reader(csvfile)
    writer = csv.writer(tempfile)
    for row in reader:
        for i, val in enumerate(row):
            try:
                float_val = float(val)
                row[i] = transform_val(float_val)
            except:
                continue
        writer.writerow(row)

shutil.move(tempfile.name, filename_out)