extern "C"
{
#include "soundio.h"
}
#include <string>
#include <iostream>
#include <chrono>
#include <mutex>
#include <q/support/literals.hpp>
#include <q/fx/biquad.hpp>
#include <vector>
#include <string>
#include <thread>

namespace q = cycfi::q;
using namespace q::literals;

#define NUM_DEVICES 2

struct SoundIoRingBuffer **ring_buffer = nullptr;
// const char *outputDevicesVals[NUM_DEVICES] = {"SoundCore", "BOOM", "Anker", "Speaker", "BenQ"};
// double outputDevicesDelays[NUM_DEVICES] = {0, 0.01, 0.01, 0.01, 0.01};
const char *outputDevicesVals[NUM_DEVICES] = {"Speaker", "BenQ"};
double outputDevicesDelays[NUM_DEVICES] = {0.02, 0.0};
double outputDeviceCounts[NUM_DEVICES] = {0};
float filterConfig[NUM_DEVICES][2] = {
    // {100, 800},
    // {1000, 100},
    // {500, 800},
};
double current_fill_count = 0;
std::chrono::system_clock::time_point start_time;

class Filterer
{
public:
    q::lowpass filter1;
    q::lowpass filter2;

    float process(float in, float weight)
    {
        return weight * filter1(in) + (1 - weight) * filter2(in);
    }

    Filterer(float a, float b) : filter1(a, 48000, 1.2), filter2(b, 48000, 1.2) {}
};
Filterer *outputFilterers[NUM_DEVICES];

// int muted_buff = 0;
// void change_muted_buff()
// {
//     while (true)
//     {
//         muted_buff = (muted_buff + 1) % NUM_DEVICES;
//         std::this_thread::sleep_for(std::chrono::seconds(2));
//     }
// }

static enum SoundIoFormat prioritized_formats[] = {
    SoundIoFormatFloat32NE,
    SoundIoFormatFloat32FE,
};
static int prioritized_sample_rates[] = {
    48000,
    44100,
    96000,
    24000,
    0,
};

__attribute__((cold))
__attribute__((noreturn))
__attribute__((format(printf, 1, 2))) static void
panic(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    vfprintf(stderr, format, ap);
    fprintf(stderr, "\n");
    va_end(ap);
    abort();
}
static int min_int(int a, int b)
{
    return (a < b) ? a : b;
}
static void write_sample_float32ne(char *ptr, double sample)
{
    float *buf = (float *)ptr;
    *buf = sample;
}

static int get_device_default_output(struct SoundIo *soundio, const char *name, bool input)
{
    if (input)
    {
        int input_count = soundio_input_device_count(soundio);
        for (int i = 0; i < input_count; i += 1)
        {
            struct SoundIoDevice *device = soundio_get_input_device(soundio, i);
            if (strstr(device->name, name) != nullptr)
            {
                soundio_device_unref(device);
                return i;
            }
            soundio_device_unref(device);
        }
    }
    else
    {
        int output_count = soundio_output_device_count(soundio);
        for (int i = 0; i < output_count; i += 1)
        {
            struct SoundIoDevice *device = soundio_get_output_device(soundio, i);
            if (strstr(device->name, name) != nullptr)
            {
                soundio_device_unref(device);
                return i;
            }
            soundio_device_unref(device);
        }
    }
    return soundio_default_output_device_index(soundio);
}

static void read_callback(struct SoundIoInStream *instream, int frame_count_min, int frame_count_max)
{
    struct SoundIoChannelArea *areas;
    int err;

    char **write_ptrs = new char *[NUM_DEVICES];
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        write_ptrs[dev] = soundio_ring_buffer_write_ptr(ring_buffer[dev]);
    }
    int free_bytes = soundio_ring_buffer_free_count(ring_buffer[0]);
    int free_count = free_bytes / instream->bytes_per_frame;
    if (frame_count_min > free_count)
        panic("ring buffer overflow");
    int write_frames = min_int(free_count, frame_count_max);
    int frames_left = write_frames;
    for (;;)
    {
        int frame_count = frames_left;
        if ((err = soundio_instream_begin_read(instream, &areas, &frame_count)))
            panic("begin read error: %s", soundio_strerror(err));
        if (!frame_count)
            break;
        if (!areas)
        {
            // Due to an overflow there is a hole. Fill the ring buffer with
            // silence for the size of the hole.
            for (int dev = 0; dev < NUM_DEVICES; dev++)
                memset(write_ptrs[dev], 0, frame_count * instream->bytes_per_frame);
            fprintf(stderr, "Dropped %d frames due to internal overflow\n", frame_count);
        }
        else
        {
            for (int frame = 0; frame < frame_count; frame += 1)
            {
                for (int ch = 0; ch < instream->layout.channel_count; ch += 1)
                {
                    for (int dev = 0; dev < NUM_DEVICES; dev++)
                    {

                        memcpy(write_ptrs[dev], areas[ch].ptr, instream->bytes_per_sample);
                        write_ptrs[dev] += instream->bytes_per_sample;
                    }
                    areas[ch].ptr += areas[ch].step;
                }
            }
        }
        if ((err = soundio_instream_end_read(instream)))
            panic("end read error: %s", soundio_strerror(err));
        frames_left -= frame_count;
        if (frames_left <= 0)
            break;
    }
    int advance_bytes = write_frames * instream->bytes_per_frame;
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        //fprintf(stderr, "Device %i written\n", dev);
        soundio_ring_buffer_advance_write_ptr(ring_buffer[dev], advance_bytes);
    }
    delete[] write_ptrs;
}

static const double PI = 3.14159265358979323846264338328;
static double seconds_offset = 0.0;

std::mutex g_i_mutex;

template <int buff>
static void write_callback(struct SoundIoOutStream *outstream, int frame_count_min, int frame_count_max)
{
    const std::lock_guard<std::mutex> lock(g_i_mutex);
    auto delay = outputDevicesDelays[buff];
    auto deadline = start_time + std::chrono::duration<double>(delay);
    auto now = std::chrono::system_clock::now();
    if (deadline > now)
    {
        struct SoundIoChannelArea *areas;
        int err;
        int frame_count = frame_count_max;
        if ((err = soundio_outstream_begin_write(outstream, &areas, &frame_count)))
        {
            panic("unrecoverable stream error: %s\n", soundio_strerror(err));
        }
        const struct SoundIoChannelLayout *layout = &outstream->layout;
        for (int frame = 0; frame < frame_count; frame += 1)
        {
            for (int channel = 0; channel < layout->channel_count; channel += 1)
            {
                write_sample_float32ne(areas[channel].ptr, 0);
                areas[channel].ptr += areas[channel].step;
            }
        }
        if ((err = soundio_outstream_end_write(outstream)))
        {
            if (err == SoundIoErrorUnderflow)
                return;
            panic("unrecoverable stream error: %s\n", soundio_strerror(err));
        }
        return;
    }

    int frame_count;
    struct SoundIoChannelArea *areas;
    int err;
    char *read_ptr = soundio_ring_buffer_read_ptr(ring_buffer[buff]);
    int fill_bytes = soundio_ring_buffer_fill_count(ring_buffer[buff]);
    int fill_count = fill_bytes / outstream->bytes_per_frame;
    if (frame_count_min > fill_count)
    {
        // Ring buffer does not have enough data, fill with zeroes.
        for (;;)
        {
            if ((err = soundio_outstream_begin_write(outstream, &areas, &frame_count)))
                panic("begin write error: %s", soundio_strerror(err));
            if (frame_count <= 0)
                return;
            for (int frame = 0; frame < frame_count; frame += 1)
            {
                for (int ch = 0; ch < outstream->layout.channel_count; ch += 1)
                {
                    memset(areas[ch].ptr, 0, outstream->bytes_per_sample);
                    areas[ch].ptr += areas[ch].step;
                }
            }
            if ((err = soundio_outstream_end_write(outstream)))
                panic("end write error: %s", soundio_strerror(err));
        }
    }
    int read_count = min_int(frame_count_max, fill_count);
    int frames_left = read_count;
    while (frames_left > 0)
    {
        int frame_count = frames_left;
        if ((err = soundio_outstream_begin_write(outstream, &areas, &frame_count)))
            panic("begin write error: %s", soundio_strerror(err));
        if (frame_count <= 0)
            break;
        for (int frame = 0; frame < frame_count; frame += 1)
        {
            float weight = (sinf(frame * 0.00005) + 1) / 2;
            for (int ch = 0; ch < outstream->layout.channel_count; ch += 1)
            {
                memcpy(areas[ch].ptr, read_ptr, outstream->bytes_per_sample);
                if (outputFilterers[buff] != nullptr)
                {
                    float val = outputFilterers[buff]->process(*((float *)areas[ch].ptr), weight);
                    write_sample_float32ne(areas[ch].ptr, val);
                }
                areas[ch].ptr += areas[ch].step;
                read_ptr += outstream->bytes_per_sample;
            }
        }
        if ((err = soundio_outstream_end_write(outstream)))
            panic("end write error: %s", soundio_strerror(err));
        frames_left -= frame_count;
    }

    if (delay == 0)
    {
        current_fill_count += read_count;
    }
    else
        outputDeviceCounts[buff] += read_count;
    soundio_ring_buffer_advance_read_ptr(ring_buffer[buff], read_count * outstream->bytes_per_frame);
}

typedef void (*WriteCallback)(SoundIoOutStream *, int frame_count_min, int frame_count_max);
static WriteCallback get_write_callback(int dev)
{
    switch (dev)
    {
    case 0:
        return write_callback<0>;
        break;
    case 1:
        return write_callback<1>;
        break;
        // case 2:
        //     return write_callback<2>;
        //     break;
        // case 3:
        //     return write_callback<3>;
        //     break;
        // case 4:
        //     return write_callback<4>;
        //     break;
        // case 5:
        //     return write_callback<5>;
        //     break;
        // case 6:
        //     return write_callback<6>;
        //     break;
        // case 7:
        //     return write_callback<7>;
        //     break;
        // case 8:
        //     return write_callback<8>;
        //     break;
        // case 9:
        //     return write_callback<9>;
        //     break;
    }
    return nullptr;
}

static void underflow_callback(struct SoundIoOutStream *outstream)
{
    static int count = 0;
    fprintf(stderr, "underflow %d\n", ++count);
}
static int usage(char *exe)
{
    fprintf(stderr, "Usage: %s [options]\n"
                    "Options:\n"
                    "  [--backend dummy|alsa|pulseaudio|jack|coreaudio|wasapi]\n"
                    "  [--in-device id]\n"
                    "  [--in-raw]\n"
                    "  [--out-device id]\n"
                    "  [--out-raw]\n"
                    "  [--latency seconds]\n",
            exe);
    return 1;
}

int main()
{
    enum SoundIoBackend backend = SoundIoBackendCoreAudio;
    bool in_raw = false;
    bool out_raw = false;
    double microphone_latency = 0.02; // seconds
    struct SoundIo *soundio = soundio_create();
    if (!soundio)
        panic("out of memory");
    int err = (backend == SoundIoBackendNone) ? soundio_connect(soundio) : soundio_connect_backend(soundio, backend);
    if (err)
        panic("error connecting: %s", soundio_strerror(err));
    soundio_flush_events(soundio);

    int in_device_index = get_device_default_output(soundio, "Soundflower (2ch)", true);
    if (in_device_index < 0)
        panic("no input device found");

    struct SoundIoDevice *in_device = soundio_get_input_device(soundio, in_device_index);
    if (!in_device)
        panic("could not get input device: out of memory");
    fprintf(stdout, "Input device: %s\n", in_device->name);

    struct SoundIoDevice **outputDevices = new SoundIoDevice *[NUM_DEVICES];
    struct SoundIoOutStream **outputStreams = new SoundIoOutStream *[NUM_DEVICES];
    ring_buffer = new SoundIoRingBuffer *[NUM_DEVICES];

    int *sample_rate;
    enum SoundIoFormat *fmt;
    const struct SoundIoChannelLayout *layout = nullptr;
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        // set up out device
        if (filterConfig[dev] && filterConfig[dev][0] != 0)
            outputFilterers[dev] = new Filterer(filterConfig[dev][0], filterConfig[dev][1]);
        else
            outputFilterers[dev] = nullptr;
        int out_device_index = get_device_default_output(soundio, outputDevicesVals[dev], false);
        struct SoundIoDevice *out_device = soundio_get_output_device(soundio, out_device_index);
        if (!out_device)
            panic("could not get output device: out of memory");
        soundio_device_sort_channel_layouts(out_device);
        std::cout << "Output device: " << out_device->name << std::endl;

        if (layout == nullptr)
        {
            layout = soundio_best_matching_channel_layout(
                out_device->layouts, out_device->layout_count,
                in_device->layouts, in_device->layout_count);
            if (!layout)
                panic("channel layouts not compatible");
            for (sample_rate = prioritized_sample_rates; *sample_rate; sample_rate += 1)
            {
                if (soundio_device_supports_sample_rate(in_device, *sample_rate) &&
                    soundio_device_supports_sample_rate(out_device, *sample_rate))
                {
                    break;
                }
            }
            if (!*sample_rate)
                panic("incompatible sample rates");
            for (fmt = prioritized_formats; *fmt != SoundIoFormatInvalid; fmt += 1)
            {
                if (soundio_device_supports_format(in_device, *fmt) &&
                    soundio_device_supports_format(out_device, *fmt))
                {
                    break;
                }
            }
            if (*fmt == SoundIoFormatInvalid)
                panic("incompatible sample formats");
        }
        else
        {
            bool hasLayout = soundio_channel_layout_equal(out_device->layouts, layout);
            bool hasSampleRate = soundio_device_supports_sample_rate(out_device, *sample_rate);
            bool hasSampleFmt = soundio_device_supports_format(out_device, *fmt);
            if (!layout)
                panic("channel layouts not compatible - sub (%i)", dev);
            if (!hasSampleRate)
                panic("incompatible sample rates - sub(%i)", dev);
            if (!hasSampleFmt)
                panic("incompatible sample formats - sub(%i)", dev);
        }
        outputDevices[dev] = out_device;
    }

    struct SoundIoInStream *instream = soundio_instream_create(in_device);
    if (!instream)
        panic("out of memory");
    instream->format = *fmt;
    instream->sample_rate = *sample_rate;
    instream->layout = *layout;
    instream->software_latency = microphone_latency;
    instream->read_callback = read_callback;
    if ((err = soundio_instream_open(instream)))
    {
        fprintf(stderr, "unable to open input stream: %s", soundio_strerror(err));
        return 1;
    }

    // setup outstream
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        struct SoundIoOutStream *outstream = soundio_outstream_create(outputDevices[dev]);
        if (!outputStreams[dev])
            panic("out of memory");
        outputStreams[dev] = outstream;
        outputStreams[dev]->format = *fmt;
        outputStreams[dev]->sample_rate = *sample_rate;
        outputStreams[dev]->layout = *layout;
        outputStreams[dev]->software_latency = microphone_latency * 20;
        outputStreams[dev]->underflow_callback = underflow_callback;
        outputStreams[dev]->write_callback = get_write_callback(dev);
        if ((err = soundio_outstream_open(outputStreams[dev])))
        {
            fprintf(stderr, "unable to open output stream: %s", soundio_strerror(err));
            return 1;
        }
    }

    //load said ring buffers
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        int capacity = microphone_latency * 60 * 4 * instream->sample_rate * instream->bytes_per_frame;
        ring_buffer[dev] = soundio_ring_buffer_create(soundio, capacity);
        if (!ring_buffer[dev])
            panic("unable to create ring buffer: out of memory");
        char *buf = soundio_ring_buffer_write_ptr(ring_buffer[dev]);
        int fill_count = microphone_latency * outputStreams[dev]->sample_rate * outputStreams[dev]->bytes_per_frame;
        memset(buf, 0, fill_count);
        soundio_ring_buffer_advance_write_ptr(ring_buffer[dev], fill_count);
    }

    if ((err = soundio_instream_start(instream)))
        panic("unable to start input device: %s", soundio_strerror(err));

    // start output streams
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        if ((err = soundio_outstream_start(outputStreams[dev])))
            panic("unable to start output device: %s", soundio_strerror(err));
    }

    start_time = std::chrono::system_clock::now();
    for (;;)
        soundio_wait_events(soundio);

    // destroy out streams
    for (int dev = 0; dev < NUM_DEVICES; dev++)
        soundio_outstream_destroy(outputStreams[dev]);

    soundio_instream_destroy(instream);
    soundio_device_unref(in_device);

    // destroy devices
    for (int dev = 0; dev < NUM_DEVICES; dev++)
    {
        delete outputFilterers[dev];
        soundio_device_unref(outputDevices[dev]);
    }

    soundio_destroy(soundio);
    delete[] ring_buffer;
    delete[] outputStreams;
    delete[] outputDevices;
    return 0;
}